package org.kauff.moumous

fun ByteArray.toHexString() = joinToString("") { "%02x".format(it) }