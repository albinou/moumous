package org.kauff.moumous

import android.app.AlertDialog
import android.bluetooth.BluetoothDevice
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import org.kauff.moumous.databinding.ViewHolderDiscoveryBinding

class DiscoveryAdapter : ListAdapter<BluetoothDevice,
                                     DiscoveryAdapter.DiscoveryViewHolder>(Companion) {

    inner class DiscoveryViewHolder(val binding: ViewHolderDiscoveryBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        init {
            binding.btAddBulb.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onAddBulb(v, binding.bluetoothDevice)
        }
    }

    companion object : DiffUtil.ItemCallback<BluetoothDevice>() {
        override fun areItemsTheSame(
            oldItem: BluetoothDevice,
            newItem: BluetoothDevice
        ): Boolean {
            return (oldItem === newItem)
        }

        override fun areContentsTheSame(
            oldItem: BluetoothDevice,
            newItem: BluetoothDevice
        ): Boolean {
            return ((oldItem.name == newItem.name) &&
                    (oldItem.address == newItem.address))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiscoveryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ViewHolderDiscoveryBinding.inflate(inflater, parent, false)
        return DiscoveryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DiscoveryViewHolder, position: Int) {
        holder.binding.bluetoothDevice = getItem(position)
        holder.binding.executePendingBindings()
    }

    private fun onAddBulb(v: View?, bluetoothDevice: BluetoothDevice?) {
        v ?: return
        bluetoothDevice ?: return
        val builder = AlertDialog.Builder(v.context)
        builder.apply {
            setTitle(R.string.alert_title_confirm_bulb_add)
            setMessage(when (bluetoothDevice.name) {
                    null -> bluetoothDevice.address
                    else -> bluetoothDevice.name + " - " + bluetoothDevice.address
            })
            setPositiveButton(R.string.alert_bt_add) { dialog, which ->
                val settings = BulbsSettings(v.context)
                settings.addFromBluetoothDevice(bluetoothDevice)
                Toast.makeText(v.context, R.string.info_bulb_added, Toast.LENGTH_SHORT).show()
            }
            setNegativeButton(android.R.string.cancel, null)
            create().show()
        }
    }
}