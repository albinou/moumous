package org.kauff.moumous

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_discovery.*

private const val REQUEST_ENABLE_BLUETOOTH = 1

class DiscoveryActivity : AppCompatActivity() {

    private val model: DiscoveryViewModel by viewModels()
    private val observer: DiscoveryLifecycleObserver by lazy {
        DiscoveryLifecycleObserver(this, model, ::requestBluetooth, ::displayWarning)
    }

    private fun displayWarning(resId: Int) {
        Toast.makeText(this, resId, Toast.LENGTH_LONG).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discovery)
        this.setTitle(R.string.title_discovery)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val adapter = DiscoveryAdapter()
        devicesList.adapter = adapter
        devicesList.layoutManager = LinearLayoutManager(this)
        model.bluetoothDevices.observe(this, Observer {
            adapter.submitList(it)
        })

        lifecycle.addObserver(observer)

        status.text = getText(R.string.discovery_status_done)
    }

    private fun requestBluetooth(intent: Intent) {
        startActivityForResult(intent, REQUEST_ENABLE_BLUETOOTH)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_ENABLE_BLUETOOTH -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        observer.onBluetoothEnabled(this)
                    }
                    Activity.RESULT_CANCELED -> {
                        observer.onBluetoothNotEnabled()
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_OK)
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
