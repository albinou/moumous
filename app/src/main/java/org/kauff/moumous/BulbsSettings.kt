package org.kauff.moumous

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.util.Log

private const val PREFERENCES_KEY_BULB_NAME = "bulb%d.name"
private const val PREFERENCES_KEY_BULB_ADDRESS = "bulb%d.address"

class BulbsSettings(context: Context) {

    private val tag = this::class.qualifiedName ?: ""
    private val sharedPreferences by lazy {
        context.getSharedPreferences(
            context.getString(R.string.shared_settings_bulbs),
            Context.MODE_PRIVATE
        )
    }
    private val bulbsCount: Int
        get() = sharedPreferences.getInt("bulbsCount", 0)

    fun addFromBluetoothDevice(bluetoothDevice: BluetoothDevice) {
        val bulbId = bulbsCount
        Log.i(tag, "Adding new bulb" + bulbId + " " + bluetoothDevice.address)
        with (sharedPreferences.edit()) {
            bluetoothDevice.name?.let {
                putString(String.format(PREFERENCES_KEY_BULB_NAME, bulbId), it)
            }
            putString(String.format(PREFERENCES_KEY_BULB_ADDRESS, bulbId), bluetoothDevice.address)
            putInt("bulbsCount", bulbId + 1)
            apply()
        }
    }

    fun getBulbs(model: BulbsViewModel): List<Bulb> {
        val size = bulbsCount
        val l = ArrayList<Bulb>(size)
        for (id in 0 until size) {
            val bluetoothDeviceName = sharedPreferences.getString(
                String.format(PREFERENCES_KEY_BULB_NAME, id), null
            )
            val bluetoothDeviceAddress = sharedPreferences.getString(
                String.format(PREFERENCES_KEY_BULB_ADDRESS, id), null
            )
            bluetoothDeviceAddress?.let {
                l.add(HoliBulb(
                    id, bluetoothDeviceName, bluetoothDeviceAddress, model
                ))
            }
        }
        return l
    }

}