package org.kauff.moumous

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner

private val TAG = BluetoothLifecycleObserver::class.java.simpleName

internal abstract class BluetoothLifecycleObserver (
    private val context: Context,
    private val requestBluetooth: (intent:Intent) -> Unit,
    private val displayWarning: (resId: Int) -> Unit
) : DefaultLifecycleObserver {

    protected val bluetoothAdapter: BluetoothAdapter? by lazy(LazyThreadSafetyMode.NONE) {
        val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        if (bluetoothAdapter === null) {
            Log.e(TAG, context.getString(R.string.alert_bluetooth_adapter_null))
            displayWarning(R.string.alert_bluetooth_adapter_null)
        }

        bluetoothAdapter?.let {
            if (it.isEnabled) {
                start(owner)
            } else {
                Log.i(TAG, context.getString(R.string.log_bluetooth_request_enable))
                requestBluetooth(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
            }
        }
    }

    fun onBluetoothEnabled(owner: LifecycleOwner) {
        Log.i(TAG, context.getString(R.string.log_bluetooth_activation_ok))
        start(owner)
    }

    fun onBluetoothNotEnabled() {
        Log.w(TAG, context.getString(R.string.log_bluetooth_activation_canceled))
        displayWarning(R.string.alert_bluetooth_activation_canceled)
    }

    protected abstract fun start(owner: LifecycleOwner)

}