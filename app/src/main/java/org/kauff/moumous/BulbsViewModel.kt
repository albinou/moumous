package org.kauff.moumous

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

private val TAG = BulbsViewModel::class.java.simpleName

class BulbsViewModel: ViewModel() {

    data class Data(
        val id: Int,
        val name: String?,
        val address: String,
        val state: Bulb.State,
        val obj: Bulb
    )

    private val _bulbs = MutableLiveData<List<Data>>(listOf())
    val bulbs: LiveData<List<Data>>
        get() = _bulbs

    fun setBulbs(bulbs: List<Bulb>) {
        val l = ArrayList<Data>(bulbs.size)
        bulbs.forEach {
            l.add(bulb2Data(it))
        }
        _bulbs.postValue(l)
    }

    fun updateBulb(bulb: Bulb) {
        val l = bulbs.value?.toMutableList()
        l?.let {
            Log.d(TAG, "Updating $bulb")
            it[bulb.id] = bulb2Data(bulb)
            _bulbs.postValue(it)
        }
    }

    private fun bulb2Data(bulb: Bulb): Data {
        return Data(bulb.id, bulb.name, bulb.address, bulb.state, bulb)
    }

}