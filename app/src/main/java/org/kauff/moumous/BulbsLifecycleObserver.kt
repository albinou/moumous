package org.kauff.moumous

import android.content.Context
import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer

private val TAG = BulbsLifecycleObserver::class.java.simpleName

internal class BulbsLifecycleObserver(
    private val context: Context,
    private val model: BulbsViewModel
) : DefaultLifecycleObserver {

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        model.bulbs.observe(owner, Observer {
            update()
        })
        update()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        model.bulbs.value?.forEach {
            val bulb = it.obj
            bulb.disconnect()
        }
        super.onDestroy(owner)
    }

    fun onBluetoothEnabled() {
        update()
    }

    private fun update() {
        Log.d(TAG, context.getString(R.string.log_bulbs_update))
        model.bulbs.value?.forEach {
            val bulb = it.obj
            bulb.connect(context)
        }
    }

}