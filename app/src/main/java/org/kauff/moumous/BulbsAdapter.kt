package org.kauff.moumous

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import org.kauff.moumous.databinding.ViewHolderBulbsBinding

class BulbsAdapter : ListAdapter<BulbsViewModel.Data, BulbsAdapter.BulbsViewHolder>(Companion) {

    inner class BulbsViewHolder(val binding: ViewHolderBulbsBinding) :
        RecyclerView.ViewHolder(binding.root), CompoundButton.OnCheckedChangeListener {

        init {
            binding.btSwitchBulb.setOnCheckedChangeListener(this)
        }

        override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
            if (isChecked) {
                binding.bulb?.let {
                    onSwitchOn(buttonView, it.obj)
                }
            } else {
                binding.bulb?.let {
                    onSwitchOff(buttonView, it.obj)
                }
            }
        }

    }

    companion object : DiffUtil.ItemCallback<BulbsViewModel.Data>() {
        override fun areItemsTheSame(
            oldItem: BulbsViewModel.Data,
            newItem: BulbsViewModel.Data
        ): Boolean {
            return (oldItem === newItem)
        }

        override fun areContentsTheSame(
            oldItem: BulbsViewModel.Data,
            newItem: BulbsViewModel.Data
        ): Boolean {
            return (oldItem == newItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BulbsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ViewHolderBulbsBinding.inflate(inflater, parent, false)
        return BulbsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BulbsViewHolder, position: Int) {
        holder.binding.bulb = getItem(position)
        holder.binding.executePendingBindings()
    }

    private fun onSwitchOn(v: View?, bulb: Bulb) {
        bulb.switchOn()
    }

    private fun onSwitchOff(v: View?, bulb: Bulb) {
        bulb.switchOff()
    }

}