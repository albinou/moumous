package org.kauff.moumous

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_home.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

private val TAG = HomeFragment::class.java.simpleName

private const val REQUEST_ENABLE_BLUETOOTH = 1

class HomeFragment : Fragment() {

    private val model: BulbsViewModel by activityViewModels()
    private val observer: BulbsLifecycleObserver? by lazy {
        context?.let { BulbsLifecycleObserver(it, model) }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = BulbsAdapter()
        bulbsRecyclerView.adapter = adapter
        bulbsRecyclerView.layoutManager = LinearLayoutManager(context)
        model.bulbs.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

        if (observer === null) {
            Log.e(TAG, getString(R.string.log_context_null))
        }
        observer?.let { lifecycle.addObserver(it) }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe
    fun onMessageEvent(@Suppress("UNUSED_PARAMETER") event: MoumousEvent.BluetoothRequestEnable) {
        Log.d(TAG, getString(R.string.log_bluetooth_request_enable))
        val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(intent, REQUEST_ENABLE_BLUETOOTH)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_ENABLE_BLUETOOTH -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        Log.i(TAG, getString(R.string.log_bluetooth_activation_ok))
                        observer?.onBluetoothEnabled()
                    }
                    Activity.RESULT_CANCELED -> {
                        Log.w(TAG, getString(R.string.log_bluetooth_activation_canceled))
                        Toast.makeText(context,
                            R.string.alert_bluetooth_activation_canceled,
                            Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

}
