package org.kauff.moumous

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*

private const val REQUEST_ACTIVITY_DISCOVERY = 1

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val model: BulbsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        bulbsLoadFromSettings()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        drawer_layout.closeDrawer(GravityCompat.START)

        when (item.itemId) {
            R.id.menu_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
            }
            R.id.menu_new_bulb -> {
                val intent = Intent(this, DiscoveryActivity::class.java)
                startActivityForResult(intent, REQUEST_ACTIVITY_DISCOVERY)
            }
            R.id.menu_settings -> {

            }
        }

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_ACTIVITY_DISCOVERY -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        bulbsLoadFromSettings()
                    }
                    Activity.RESULT_CANCELED -> {
                        Toast.makeText(this, R.string.alert_activity_discovery_canceled,
                            Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun bulbsLoadFromSettings() {
        model.setBulbs(BulbsSettings(this).getBulbs(model))
    }

}
