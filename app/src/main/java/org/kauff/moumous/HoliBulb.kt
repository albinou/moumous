package org.kauff.moumous

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import java.util.*

private val TAG = HoliBulb::class.java.simpleName

class HoliBulb(
    id: Int,
    name: String?,
    address: String,
    model: BulbsViewModel
) : Bulb(id, name, address, model) {

    private var bulbsBluetoothLeService: BulbsBluetoothLeService? = null

    private val bluetoothGattServiceHardwareUUID =
        UUID.fromString("00ff5502-3c25-45cb-99dc-1754766b829a")
    private val bluetoothGattServiceApplicationUUID =
        UUID.fromString("01ff5502-3c25-45cb-99dc-1754766b829a")
    private val bluetoothGattServiceBootloaderUUID =
        UUID.fromString("02ff5502-3c25-45cb-99dc-1754766b829a")

    // Hardware characteristics
    private val bluetoothGattCharacteristicOffUUID =
        UUID.fromString("044993e6-5eed-439a-9497-9e4086539756")

    // Application characteristics
    private val bluetoothGattCharacteristicPingUUID =
        UUID.fromString("f04993e6-5eed-439a-9497-9e4086539756")
    private val bluetoothGattCharacteristicColorUUID =
        UUID.fromString("f14993e6-5eed-439a-9497-9e4086539756")


    private inner class BluetoothLeServiceConnection(private val context: Context) : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as BulbsBluetoothLeService.BulbsBluetoothLeBinder
            Log.d(TAG, "Bound to the BulbsBluetoothLeService :-)")
            bulbsBluetoothLeService = binder.getService()
            connect(context)
        }
        override fun onServiceDisconnected(name: ComponentName?) {
            bulbsBluetoothLeService = null
        }
    }

    private fun bindBluetoothLeService(context: Context) {
        Intent(context, BulbsBluetoothLeService::class.java).also { intent ->
            context.bindService(intent,
                BluetoothLeServiceConnection(context),
                Context.BIND_AUTO_CREATE)
        }
    }

    override fun connect(context: Context) {
        if (state != State.DISCONNECTED) {
            Log.w(TAG, "Attempting to connect while $this is already connected or connecting")
            return
        }
        Log.i(TAG, "Connecting to $this")
        if (bulbsBluetoothLeService == null) {
            bindBluetoothLeService(context)
        } else {
            bulbsBluetoothLeService?.let { service ->
                super.connect(context)
                service.connect(this)
            }
        }
    }

    override fun disconnect() {
        Log.i(TAG, "Disconnecting from $this")
        bulbsBluetoothLeService?.disconnect(this)
        super.disconnect()
    }

    override fun switchOn() {
        super.switchOn()
        bulbsBluetoothLeService?.let { service ->
            service.writeGattCharacteristic(
                this,
                bluetoothGattServiceApplicationUUID,
                bluetoothGattCharacteristicPingUUID,
                byteArrayOfInts(
                    0x00, 0x00, 0x01, 0x00
                )
            )
            service.writeGattCharacteristic(
                this,
                bluetoothGattServiceApplicationUUID,
                bluetoothGattCharacteristicColorUUID,
                byteArrayOfInts(
                    0x00, 0x00, 0x00, 0x00, 0x00,
                    0xff, 0x00, 0x00,
                    0xff, 0x00
                )
            )
        }
    }

    override fun switchOff() {
        super.switchOff()
        bulbsBluetoothLeService?.writeGattCharacteristic(
            this,
            bluetoothGattServiceHardwareUUID,
            bluetoothGattCharacteristicOffUUID,
            byteArrayOfInts(
                0x00
            )
        )
    }

    private fun byteArrayOfInts(vararg ints: Int): ByteArray {
        return ByteArray(ints.size) { pos ->
            ints[pos].toByte()
        }
    }

}