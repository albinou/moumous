package org.kauff.moumous

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DiscoveryViewModel : ViewModel() {

    private val _bluetoothDevices = MutableLiveData<List<BluetoothDevice>>(listOf())
    val bluetoothDevices: LiveData<List<BluetoothDevice>>
        get() = _bluetoothDevices

    fun addNewDevice(dev: BluetoothDevice) {
        val oldDevices = _bluetoothDevices.value ?: return
        if (!oldDevices.contains(dev)) {
            _bluetoothDevices.postValue(oldDevices.plusElement(dev))
        }
    }

}