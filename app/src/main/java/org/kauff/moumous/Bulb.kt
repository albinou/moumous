package org.kauff.moumous

import android.content.Context
import android.util.Log

private val TAG = Bulb::class.java.simpleName

abstract class Bulb(
    val id: Int,
    val name: String?,
    val address: String,
    private val model: BulbsViewModel
) {

    enum class State {
        DISCONNECTED,
        CONNECTING,
        CONNECTED
    }

    var state: State = State.DISCONNECTED
        protected set(value) {
            if (field != value) {
                Log.d(TAG, "$this switches from $field to $value")
                field = value
                model.updateBulb(this)
            }
        }

    override fun toString(): String {
        return "${javaClass.simpleName}(id=${id}, name=${name}, address=${address}, state=${state})"
    }

    open fun connect(context: Context) {
        state = State.CONNECTING
    }

    open fun disconnect() {
        state = State.DISCONNECTED
    }

    open fun onConnected() {
        state = State.CONNECTED
    }

    open fun onDisconnected() {
        state = State.DISCONNECTED
    }

    open fun switchOn() {
        Log.i(TAG, "Switching on $this")
    }

    open fun switchOff() {
        Log.i(TAG, "Switching off $this")
    }

}