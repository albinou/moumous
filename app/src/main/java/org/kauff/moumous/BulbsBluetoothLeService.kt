package org.kauff.moumous

import android.app.Service
import android.bluetooth.*
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import org.greenrobot.eventbus.EventBus
import java.util.*

private val TAG = BulbsBluetoothLeService::class.java.simpleName

class BulbsBluetoothLeService : Service() {

    private var bluetoothAdapter: BluetoothAdapter? = null

    private class GattWriteRequest(
        val serviceUUID: UUID,
        val characteristicUUID: UUID,
        val data: ByteArray
    )
    private class BulbData(
        val bluetoothGatt: BluetoothGatt,
        val pendingGattWriteRequests: Queue<GattWriteRequest>
    )
    private val bulbsMap = mutableMapOf<Bulb, BulbData>()

    private inner class BulbBluetoothGattCallback(
        private val bulb: Bulb,
        private val pendingGattWriteRequests: Queue<GattWriteRequest>
    ) : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    Log.i(TAG, getString(R.string.log_gatt_connected))
                    Log.d(TAG, "Discovering services")
                    gatt?.discoverServices()
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    Log.i(TAG, getString(R.string.log_gatt_disconnected))
                    bulb.onDisconnected()
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    Log.d(TAG, getString(R.string.log_gatt_services_discovery_success))
                    bulb.onConnected()
                }
                else -> {
                    Log.w(TAG, getString(R.string.log_gatt_services_discovery_error, status))
                }
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    Log.d(TAG, getString(R.string.log_gatt_characteristic_read_success,
                        characteristic.toString()))
                }
                else -> {
                    Log.w(TAG, getString(R.string.log_gatt_characteristic_read_error,
                        characteristic.toString(), status))
                }
            }
        }

        override fun onCharacteristicWrite(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            Log.d(TAG, "GATT characteristic $characteristic successfully read on $bulb")
            pendingGattWriteRequests.poll()
            if (gatt != null) {
                pollPendingGattWriteRequests(
                    gatt,
                    pendingGattWriteRequests
                )
            }
        }
    }

    private val binder = BulbsBluetoothLeBinder()

    inner class BulbsBluetoothLeBinder : Binder() {
        fun getService(): BulbsBluetoothLeService {
            return this@BulbsBluetoothLeService
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onCreate() {
        super.onCreate()
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter
        if (bluetoothAdapter == null) {
            Log.e(TAG, getString(R.string.alert_bluetooth_adapter_null))
        }
        bluetoothAdapter?.let { adapter ->
            if (!adapter.isEnabled) {
                Log.i(TAG, getString(R.string.log_bluetooth_request_enable))
                EventBus.getDefault().post(MoumousEvent.BluetoothRequestEnable())
            }
        }
    }

    private fun isReady(): Boolean {
        return bluetoothAdapter?.isEnabled ?: false
    }

    private fun notReady(): Boolean {
        return (!isReady())
    }

    fun connect(bulb: Bulb) {
        if (notReady()) return
        Log.d(TAG, "Connecting to $bulb")
        val bulbData = bulbsMap[bulb]
        if (bulbData == null) {
            Log.d(TAG, "First connection to $bulb")
            val bluetoothDevice = bluetoothAdapter?.getRemoteDevice(bulb.address)
            if (bluetoothDevice == null) {
                Log.e(TAG, "Can't get the Bluetooth remote device of $this")
                return
            }
            val pendingQueue = LinkedList<GattWriteRequest>()
            val bluetoothGatt = bluetoothDevice.connectGatt(
                this,
                false,
                BulbBluetoothGattCallback(bulb, pendingQueue)
            )
            bluetoothGatt?.let { gatt ->
                bulbsMap[bulb] = BulbData(gatt, pendingQueue)
            }
        } else {
            Log.d(TAG, "Reconnecting to $bulb")
            bulbData.bluetoothGatt.connect()
        }
    }

    fun disconnect(bulb: Bulb) {
        Log.d(TAG, "Disconnecting from $bulb")
        val bulbData = bulbsMap[bulb]
        if (bulbData == null) {
            Log.w(TAG, "Attempt to disconnect $bulb while not connected")
            return
        }
        bulbData.bluetoothGatt.disconnect()
    }

    fun writeGattCharacteristic(bulb: Bulb, serviceUUID: UUID, characteristicUUID: UUID, data: ByteArray) {
        val bulbData = bulbsMap[bulb]
        if (bulbData == null) {
            Log.w(TAG, "Attempt to write data to $bulb while not connected")
            return
        }
        bulbData.pendingGattWriteRequests.add(
            GattWriteRequest(
                serviceUUID,
                characteristicUUID,
                data
            )
        )
        pollPendingGattWriteRequests(bulbData.bluetoothGatt, bulbData.pendingGattWriteRequests)
    }

    private fun pollPendingGattWriteRequests(bluetoothGatt: BluetoothGatt, pendingGattWriteRequests: Queue<GattWriteRequest>) {
        val request = pendingGattWriteRequests.peek() ?: return
        val serviceUUID = request.serviceUUID
        val characteristicUUID = request.characteristicUUID
        val data = request.data
        if (pendingGattWriteRequests.size == 1) {
            Log.d(
                TAG,
                "Writing 0x${data.toHexString()} to service $serviceUUID, " +
                        "characteristic $characteristicUUID " +
                        "of ${bluetoothGatt.device.address}"
            )
            val bluetoothGattService = bluetoothGatt.getService(serviceUUID)
            val bluetoothGattCharacteristic =
                bluetoothGattService.getCharacteristic(characteristicUUID)
            bluetoothGattCharacteristic.value = data
            bluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic)
        } else {
            Log.d(TAG, "Delaying the write of data to ${bluetoothGatt.device.address}")
        }
    }

}