package org.kauff.moumous

import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LifecycleOwner

private val TAG = DiscoveryLifecycleObserver::class.java.simpleName

private const val SCAN_PERIOD: Long = 10000

internal class DiscoveryLifecycleObserver (
    private val context: Context,
    private val model: DiscoveryViewModel,
    requestBluetooth: (intent: Intent) -> Unit,
    displayWarning: (resId: Int) -> Unit
): BluetoothLifecycleObserver(context, requestBluetooth, displayWarning) {

    inner class MyScanCallback: ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            result?.device?.let {
                model.addNewDevice(it)
            }
        }
    }

    private val bluetoothLeScanner: BluetoothLeScanner? by lazy(LazyThreadSafetyMode.NONE) {
        bluetoothAdapter?.bluetoothLeScanner
    }
    private val scanCallback = MyScanCallback()
    private var btScanning = false
    private val handler = Handler(Looper.getMainLooper())

    override fun start(owner: LifecycleOwner) {
        startDiscovery()
    }

    override fun onStop(owner: LifecycleOwner) {
        stopDiscovery()
        super.onStop(owner)
    }

    fun startDiscovery() {
        Log.i(TAG, context.getString(R.string.log_discovery_start))
        handler.postDelayed(::stopDiscovery, SCAN_PERIOD)
        btScanning = true
        bluetoothLeScanner?.startScan(scanCallback)
    }

    fun stopDiscovery() {
        Log.i(TAG, context.getString(R.string.log_discovery_stop))
        handler.removeCallbacks(::stopDiscovery)
        btScanning = false
        bluetoothLeScanner?.stopScan(scanCallback)
    }

}