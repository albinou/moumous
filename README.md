# Moumous

Moumous is an Android application allowing to control the Holi SleepCompanion.

The SleepCompanion is a LED bulb controlled with Bluetooth LE.
It was manufactured by Holi, a French company that bankrupted.
The product description can be found by following these links:

- [Official Website (French)](https://www.holi.io/?portfolio=sleepcompanion-lampoule-qui-vous-reveille-les-neurones)
- [An English overview](https://www.hobbr.com/holi-sleep-companion)

The original application from Holi is still available on [the Google Play store](https://play.google.com/store/apps/details?id=com.holimotion.android.sleepcompanion) but is of course not maintained anymore.
Then, Moumous objective is to fight against obsolescence and to bring more features to the user.

## Application requirements

- Android >= 5.0
- the application requires the following permissions: FIXME

## Installation

### From F-Droid

Coming soon...

### From Android play

Coming soon...

### From sources

Coming soon...

## Development status

### Supported features

None at this time, it's just the beginning!

### TODO

## Development resources

### Testing & Issues

Please share with me any issue you could encounter with your own device.
Issues can be created [here](https://gitlab.com/albinou/moumous/issues).

### SleepCompanion reverse-engineering

This project is developed using the reverse-engineering performed on [this other project](https://gitlab.com/albinou/python-sleep360).
The BLE protocol can be found into [this file](https://gitlab.com/albinou/python-sleep360/-/blob/master/BLE-protocol.md).

## FAQ

### Why Moumous?

Just because I love Houmous and wanted a fancy name :)

### What about controlling several Sleepcompanions or even other bulbs?

This Moumous project is written with genericity in mind so let's hope the Android app will be able to control several SleepCompanion devices and even devices from other brands in the future.

Please contact me if you want to add the support for your bulb!
